<?php

declare(strict_types = 1);

/**
 * @file
 * Functions to support theming.
 */

function step_by_step_theme() {

  return [
    'step_by_step_part_of_block' => [
      'variables' => [
        'label' => NULL,
        'url' => NULL,
      ],
      'render element' => 'children',
    ],
    'views_view_list__step_by_step_navigation__prev_next' => [
      'base hook' => 'views_view_list',
    ],
  ];
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function step_by_step_node_insert($entity) {

  step_by_step_node_update($entity);
}

/**
 * Implements hook_ENTITY_TYPE_update().
 *
 * The step_by_step_overview and step_by_step_page content types reference each
 * other.  When a step_by_step_page type node is updated, find its
 * step_by_step_overview node.  Then make sure this step_by_step_overview node
 * refers back to the step_by_step_page node.
 */
function step_by_step_node_update($entity) {

  $is_not_step_by_step_page_node = ($entity->bundle() !== 'step_by_step_page');
  if ($is_not_step_by_step_page_node) {
    return;
  }

  $step_by_step_page_node = $entity;
  $step_by_step_page_nid = $step_by_step_page_node->id();

  $step_by_step_overview_node = $step_by_step_page_node->field_step_parent->entity ?? FALSE;
  if (empty($step_by_step_overview_node)) {
    return;
  }

  try {
    $has_ref_to_this_step_by_step_page = array_filter($step_by_step_overview_node->field_step_by_step_pages->referencedEntities(), function ($page_node) use ($step_by_step_page_nid): bool {
      return $page_node->id() === $step_by_step_page_nid;
    });
    if ($has_ref_to_this_step_by_step_page) {
      return;
    }

    $step_by_step_overview_node->field_step_by_step_pages->appendItem($step_by_step_page_nid);
    $step_by_step_overview_node->save();
  }
  catch (Exception $e) {
    Drupal::service('logger.factory')->get('step-by-step')->error($e->getMessage());
  }
}

function step_by_step_preprocess_views_view_list(array &$variables) {

  $view            = $variables['view'];
  $view_id         = $view->id();
  $view_display_id = $view->current_display;

  $is_step_by_step_nav             = ($view_id === 'step_by_step_navigation');
  $is_step_by_step_nav_block       = ($view_display_id === 'steps');
  $is_step_by_step_prev_next_block = ($view_display_id === 'prev_next');

  $current_nid = $is_step_by_step_nav ? $view->args[0] : '';

  if ($is_step_by_step_nav and $is_step_by_step_prev_next_block) {
    cambridge_step_by_step_find_prev_next_steps($current_nid, $variables);
  }
  elseif ($is_step_by_step_nav and $is_step_by_step_nav_block) {
    cambridge_step_by_step_mark_current_step($current_nid, $variables);

  }

  if ($is_step_by_step_nav) {
    $variables['view']->element['#attached']['library'][] = 'step_by_step/step-by-step';
  }
}

function cambridge_step_by_step_find_prev_next_steps(string $current_nid, array &$variables): void {

  $prev_step_index = -1;
  $next_step_index = -1;

  foreach ($variables['rows'] as $key => $row) {
    $row_nid = $row['content']['#row']->node_field_data_node__field_step_by_step_pages_nid ?? '-1';
    $is_current_step = ($row_nid === $current_nid);

    if ($is_current_step) {
      $prev_step_index = $key - 1;
      $next_step_index = $key + 1;

      break;
    }
  }

  $variables['has_prev_step'] = array_key_exists($prev_step_index, $variables['rows']);
  $variables['has_next_step'] = array_key_exists($next_step_index, $variables['rows']);
  $variables['prev_step_nid'] = $variables['rows'][$prev_step_index]['content']['#row']->node_field_data_node__field_step_by_step_pages_nid ?? -1;
  $variables['next_step_nid'] = $variables['rows'][$next_step_index]['content']['#row']->node_field_data_node__field_step_by_step_pages_nid ?? -1;
  $variables['prev_step_link_text'] = t('Previous Step');
  $variables['prev_step_index'] = $prev_step_index;
  $variables['next_step_index'] = $next_step_index;

  $is_first_step = ($next_step_index === 1);
  $variables['next_step_link_text'] = $is_first_step ? t('Next Step') : t('Next Step');

  $node_storage = \Drupal::entityTypeManager()->getStorage('node');
  if ($variables['has_next_step']) {
    $variables['next_step_title'] = $node_storage->load($variables['next_step_nid'])->title->value;
  }
  if ($variables['has_prev_step']) {
    $variables['prev_step_title'] = $node_storage->load($variables['prev_step_nid'])->title->value;
  }
}

/**
 * Mark the row corresponding to the current Step by step page node.
 */
function cambridge_step_by_step_mark_current_step(string $current_nid, array &$variables): void {

  foreach ($variables['rows'] as &$row) {
    $row_nid = $row['content']['#row']->node_field_data_node__field_step_by_step_pages_nid ?? '-1';

    $is_current_step = ($row_nid === $current_nid);
    if ($is_current_step) {
      $row['attributes']['class'][] = 'step--active';
      break;
    }
  }
}
